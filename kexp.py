
import datetime
import urllib2
from bs4 import BeautifulSoup
from rfc3339 import rfc3339

class KEXPEvent:
    
    def __init__(self):
        self.name = ""
        self.date = "TBD"
        self.broadcast_only = False
        self.open_to_public = False

    def __str__(self):
        return "%s on %s (%r, %r)" % (self.name, self.date, self.broadcast_only, self.open_to_public)

    def google_cal_format(self):
        return {
            "summary": self.name,
            "location": "KEXP, 472 1st Ave N, Seattle, WA 98109",
            "description": "",
            "start": {
                "dateTime": rfc3339(self.date),
                "timeZone": "US/Pacific",
            },
            "end": {
                "dateTime": rfc3339(self.date + datetime.timedelta(hours=1)),
                "timeZone": "US/Pacific"
            },
            "reminders": {
                "useDefault": False,
                "overrides": [
                    {
                        "method": "popup",
                        "minutes": 60
                    },
                    {
                        "method": "popup",
                        "minutes": 4320     # 3 days
                    }
                ]
            }
        }



def get_events():
    response = urllib2.urlopen("http://kexp.org/events/instudio")
    soup = BeautifulSoup(response.read(), "html.parser")
    for element in soup.find_all("b"):  
        try:
            event = KEXPEvent()
            event.name = element.contents[0].encode("utf-8").strip()
            event.date = datetime.datetime.strptime(str(element.next_sibling).strip(), "%A, %B %d, %Y, %I:%M %p")
            event.broadcast_only = "BROADCAST ONLY" in event.name
            event.open_to_public = "OPEN TO THE PUBLIC" in event.name
            yield event
        except ValueError as err:
             pass
        