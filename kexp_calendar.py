
from __future__ import print_function
import os
import datetime
import httplib2
from apiclient import discovery
from oauth2client import client
from oauth2client import tools
from oauth2client.file import Storage

SCOPES = 'https://www.googleapis.com/auth/calendar'
CLIENT_SECRET_FILE = 'client_secret.json'
APPLICATION_NAME = 'KEXP Bot'

class KEXPCalendarNotFound(BaseException):
    
    def __init__(self, calendar_name):
        super(KEXPCalendarNotFound, self).__init__("Could not find calendar '%s'" % calendar_name)
        self.calendar_name = calendar_name

class KEXPCalendar:

    def __init__(self, calendar_name):
        self.calendar_name = calendar_name
        self.credentials = self._get_credentials()
        self.http = self.credentials.authorize(httplib2.Http())
        self.service = discovery.build('calendar', 'v3', http=self.http)
        self.calendar_id = self._get_calendar_id()
        if self.calendar_id == None:
            raise KEXPCalendarNotFound(calendar_name)
        self.events = self._get_events()

    def _get_credentials(self):
        """Gets valid user credentials from storage.

        If nothing has been stored, or if the stored credentials are invalid,
        the OAuth2 flow is completed to obtain the new credentials.

        Returns:
            Credentials, the obtained credential.
        """
        home_dir = os.path.expanduser('~')
        credential_dir = os.path.join(home_dir, '.credentials')
        if not os.path.exists(credential_dir):
            os.makedirs(credential_dir)
        credential_path = os.path.join(credential_dir, 'calendar-python-quickstart.json')
        store = Storage(credential_path)
        credentials = store.get()
        if not credentials or credentials.invalid:
            flow = client.flow_from_clientsecrets(CLIENT_SECRET_FILE, SCOPES)
            flow.user_agent = APPLICATION_NAME
            credentials = tools.run_flow(flow, store, None)
            print('Storing credentials to ' + credential_path)
        return credentials

    def delete_credentials(self):
        raise NotImplementedError()

    def _get_events(self):
        now = datetime.datetime.utcnow().isoformat() + 'Z'
        eventsResult = self.service.events().list(
            calendarId=self.calendar_id,
            timeMin=now,
            singleEvents=True,
            orderBy='startTime').execute()
        return eventsResult.get('items', [])
    
    def delete_events(self):
        for event in self.events:
            self.service.events().delete(calendarId=self.calendar_id, eventId=event["id"]).execute()

    def _get_calendar_id(self):
        for calendar in self._get_calendar_list():
            if calendar["summary"] == self.calendar_name:
                return calendar["id"]
        return None
    
    def _get_calendar_list(self):
        page_token = None
        while True:
            calendar_list = self.service.calendarList().list(pageToken=page_token).execute()
            for calendar_list_entry in calendar_list['items']:
                yield calendar_list_entry
            page_token = calendar_list.get('nextPageToken')
            if not page_token:
                break

    def has_event(self, kexp_event):
        for event in self.events:
            event_name = event['summary'].encode('UTF-8').strip()
            if (kexp_event.name == event_name):
                return True
        return False

    def add_event(self, event):
        self.service.events().insert(calendarId=self.calendar_id, body=event.google_cal_format()).execute()
