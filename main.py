
import argparse
import kexp
from kexp_calendar import *

def main(calendar_name):
    print("Updating KEXP Calendar...")
    try:
        calendar = KEXPCalendar(calendar_name)
        # print("Deleting events...")
        # calendar.delete_events()
        # print("Done deleting events...")
        now = datetime.datetime.now()
        for event in kexp.get_events():
            if (event.date > now) and (not calendar.has_event(event)):
                calendar.add_event(event)
                print("Added " + str(event))
    except Exception as err:
        print(err.message)
        return -1
    finally:
        print("Done updating KEXP Calendar...")
    return 0

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='KEXP Bot')
    parser.add_argument("--calendar", required=True, type=str, help="Name of the calendar to add events to.")
    args = parser.parse_args()
    main(args.calendar)
